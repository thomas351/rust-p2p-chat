# https://stackoverflow.com/questions/29008127/why-are-rust-executables-so-huge
# https://github.com/johnthagen/min-sized-rust

# first we applied lot's of settings in the [profile.release] block on Crago.toml.

# with those, the default release build gives 319kb:
#cargo build --release

# using this instead compiles hello world to 311kb on my machine:
#RUSTFLAGS="-Zlocation-detail=none" cargo +nightly build --release

# recompile rust's lib-std optimized for size and stripping parts not used by us
#RUSTFLAGS="-Zlocation-detail=none" cargo +nightly build -Z build-std=std,panic_abort --target x86_64-unknown-linux-gnu --release

# also Remove panic String Formatting with panic_immediate_abort
RUSTFLAGS="-Zlocation-detail=none" cargo +nightly build -Z build-std=std,panic_abort -Z build-std-features=panic_immediate_abort --target x86_64-unknown-linux-gnu --profile opti-release

# resulting binary size of hello world sample = 31kb:
ls -alih target/x86_64-unknown-linux-gnu/opti-release/rust-p2p-chat

echo "compressing binary down using epx (hello world @ my linux box = ~50% -> 15kb)"
upx --best --lzma target/x86_64-unknown-linux-gnu/opti-release/rust-p2p-chat

ls -alih target/x86_64-unknown-linux-gnu/opti-release/rust-p2p-chat